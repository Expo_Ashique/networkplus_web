
from django.conf import settings
from django.core import serializers
from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from base.models import *
import json
from django.http import HttpResponse
import base64
import hashlib
from django.core.files import File
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import ContentFile
import uuid
import time
import requests
import smtplib
from urllib.request import urlopen
from email.mime.text import MIMEText

from bs4 import BeautifulSoup
import urllib.parse

import numpy as np
import base64
import urllib
from PIL import Image
from html.parser import HTMLParser


def card(request):

    url2 = settings.API_SERVER + '/api/get_category_list/'
    params2 = {}
    r2 = requests.get(url2, params=params2)
    result2 = r2.json()

    session = request.session["session_id"]

    api_server = '' + settings.API_SERVER + '/api/post/'

    return render(request, 'base/card.html', {
        'category_data': result2,
        'session': session,
        'api_server': api_server
    })


def Ajax_post_cardlist(request):
    session = request.session["session_id"]

    card_list = request.POST['card_list']
    category_ids= request.POST['category_ids']


    data = {}
    data["post_title"] = request.POST['post_title']
    data["hash_tag"] = request.POST['hash_tag']
    data["link[]"] = []
    data["category_id[]"] = []
    data["description[]"] = []
    data["content[]"] = []
    data["content_type[]"] = []
    data["post_privacy"] = "public"
    data["is_publish"] = request.POST['is_publish']
    data["session"] = session

    for ci in json.loads(category_ids):
        data["category_id[]"].append(ci)



    for cl in json.loads(card_list):
        data["link[]"].append(cl["url"])
        data["description[]"].append(cl["text"])

        data["content[]"].append(cl["file"])
        data["content_type[]"].append(cl["cardType"])


    url = ''+ settings.API_SERVER + '/api/post/'

    r = requests.post(url, data=data)
    result = r.json()


    if result == "-1":
        return HttpResponse("-1")

    return HttpResponse(result["post_id"])



def single_post(request, post_id):
    session = request.session.get("session_id",None)

    url = '' + settings.API_SERVER + '/api/post/'
    params = {'session': session, 'post_id': post_id}
    r = requests.get(url, params=params)
    result = r.json()


    card_data = result["data"][0]["card_data"]
    user_data = result["data"][0]["user_data"][0]
    title = result["data"][0]["title"]
    post_added_date = result["data"][0]["post_added_date"]
    post_privacy = result["data"][0]["post_privacy"]



    return render(request, 'base/post_details.html', {
        "user_data":user_data,
        "card_data":card_data,
        "title":title,
        "post_added_date":post_added_date,
        "post_privacy":post_privacy,

    })




def getUrlByAjax(request):
    url = request.POST['url']

    p = urllib.parse.urlparse(url, 'http')
    netloc = p.netloc or p.path
    path = p.path if p.netloc else ''
    if not netloc.startswith('www.'):
        netloc = 'www.' + netloc

    p = urllib.parse.ParseResult('http', netloc, path, *p[3:])
    # print(p.geturl())

    target_url = p.geturl()

    try:
        html = urlopen(target_url)
    except:
        return HttpResponse("-1")

    soup = BeautifulSoup(html, 'html.parser')


    title = ""

    try:
        for item in soup.find_all('title'):
            title = item.get_text()
            break
    except:
        pass

    image = ""
    try:
        for item in soup.find_all('img'):
            image = item["src"]
            if image.find("http://") == -1:
                image = target_url + "/" + image
            break
    except:
        pass

    description = ""
    try:
        for item in soup.find_all("meta", {"name": "description"}):
            description = item["content"]
            break
    except:
        pass

    try:
        image = img2base64(image)
    except:
        pass

    output = {
        "title": title,
        "image": image,
        "description": description,
        "url": target_url
    }

    return HttpResponse(json.dumps(output))


# Convert image to base64 thumbnail
def img2base64(img_link):
    with open("/tmp/img_file.jpg", "wb") as f:
        f.write(urllib.request.urlopen(img_link).read())
    tmp_img = np.asarray(Image.open("/tmp/img_file.jpg"))
    with open("/tmp/img_file.jpg", "rb") as img:
        thumb_string = base64.b64encode(img.read())
    base64out = "data:image/jpeg;base64," + str(thumb_string)[2:-1]
    return (base64out)


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)
