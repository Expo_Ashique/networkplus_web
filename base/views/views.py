from django.conf import settings
from django.core import serializers
from django.shortcuts import render
from django.contrib.auth import logout
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from base.models  import *
import json
from django.http import HttpResponse
import base64
import hashlib
from django.core.files import File
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import ContentFile
import uuid
import time
import requests
import smtplib
from email.mime.text import MIMEText



def home(request):

    return HttpResponse('Wellcome to Network plus')

def reset_password(request, security_code):

    if request.method=="GET":

        return render(request, 'base/reset_password.html')

    if request.method=="POST":
        password= request.POST['password']
        confirm_password= request.POST['confirm_password']

        if password!=confirm_password:
            return render(request, 'base/reset_password.html',{"error_message":"Password and Confirm Password Don't match"})

        url = '' + settings.API_SERVER + '/api/reset-password/'

        params={
	"security_code":security_code,
	"new_password":password
}

        r = requests.post(url, params=params)
        print(r)
        result = r.json()
        print(result)

        if result['status']=='success':
            return render(request, 'base/reset_password.html',
                          {"message": "Password Reset Successful. Please login with new information."})

        else:
            return render(request, 'base/reset_password.html',
                          {"error_message": "Link Expired. Or Already Used."})



